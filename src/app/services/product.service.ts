import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productListPromise: any;
  obj = {
    brand: "",
    description: "POTATO, FRENCH-FRY 5/16 THIN CUT SKIN-ON TFF EXTRA-LONG-FANCY",
    foodClass: "Grocery, Ref & Fzn",
    groupName: "Group 1",
    id: "1583138",
    image: "assets/potato.jpg",
    lineNo: null,
    lists: [
      "Master (SL)",
      "test203",
      "tset202"
    ],
    orderHistory: "No Product History",
    priceCases: "",
    priceEaches: "",
    locallySourced: false,
    discontinued: false,
    specificTime: false,
    specialOrder: false,
    productAttributes: [],
    productNote: "",
    quantity: "",
    short: "",
    todo: "Remove Search For Subs",
    updateCases: "",
    updateEaches: "",
    usfStorage: ["FROZEN 0"]
  }
  productList: any = [];
  unfilteredList;
  copyList;

  constructor(private http: HttpClient) {

  }

  //Initial approach to extract data through LOCAL JSON
  getProductList() {
    let that = this;
    return new Promise((resolve, reject) => {
      this.http.get('assets/uniqueProducts.json')
        .subscribe((data) => {
          this.productList = _.cloneDeep(data['products']);
          this.unfilteredList = _.clone(data['products']);
          this.copyList = _.clone(data['products']);
          resolve(that.productList);
        });
    })

  }
  getProductById(id) {
    var product = (_.find(this.productList, ['id', id]));
    return (_.clone(product));
  }
  getJSFdata() {
    var that = this;
    return new Promise((resolve, reject) => {
      this.http.get("http://localhost:7101/order/jersey/MyShoppingListService")
      .subscribe((data: any[]) => {
        _.each(data, function (value) {
          if(value.description!="NONE"){
          var temp = _.cloneDeep(that.obj);
          temp.brand = value.brandName;
          temp.description = value.description;
          temp.short = value.productName;
          temp.priceCases = value.casePrice;
          temp.priceEaches = value.eachPrice;
          temp.specialOrder = value.specialOrderProduct;
          temp.discontinued = value.discontinued;
          temp.locallySourced = value.locallySourced;
          temp.image = value.image;
          temp.quantity = value.packSize;
          temp['promotion'] = value.promotion;
          temp.id=value.itemNumber;
          temp.lineNo=+value.displayLineNumber;
          temp.groupName=value.groupName;
          that.productList.push(temp);
          }
        });
        that.productList=_.uniqBy(that.productList,'id');
        //that.productList=_.sortBy(that.productList,'lineNo');
        //  this.productList = _.cloneDeep(normal);
        this.unfilteredList = _.clone(that.productList);
        this.copyList = _.clone(that.productList);
        resolve(_.cloneDeep(that.productList));
      });
    });
  }
}