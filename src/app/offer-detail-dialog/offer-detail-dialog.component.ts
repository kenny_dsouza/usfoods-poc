import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';

@Component({
  selector: 'app-offer-detail-dialog',
  templateUrl: './offer-detail-dialog.component.html',
  styleUrls: ['./offer-detail-dialog.component.scss']
})
export class OfferDetailDialogComponent implements OnInit {
promotion:any;
  constructor(
    public dialogRef: MatDialogRef<OfferDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any) {}
   close(){

    this.dialogRef.close();
   }
    ngOnInit() {
      this.promotion=this.data.promotion;
    }
}


