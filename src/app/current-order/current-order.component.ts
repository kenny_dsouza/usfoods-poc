import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DatePoDialogComponent } from '../date-po-dialog/date-po-dialog.component';
import { ProductService } from 'src/app/services/product.service';
@Component({
  selector: 'app-current-order',
  templateUrl: './current-order.component.html',
  styleUrls: ['./current-order.component.scss']
})
export class CurrentOrderComponent implements OnInit {
  currentOrderList = [];
  quickEntryProduct: any;
  productFound = true;
  orderList: any={};
  totalPrice = 0;
  disableCheck=true;
  constructor(private dialog: MatDialog, private productService: ProductService) { }
  ngOnInit() {
    this.orderList.deliveryDate=new Date();
  }
  openDialog(type): Observable<any> {
    const dialogRef = this.dialog.open(DatePoDialogComponent, {
      width: '450px',
      data: {
        dialogType: type,
        orderObject: this.orderList
      }
    });
    return dialogRef.afterClosed();
  }
  updateCurrentOrderFromProduct(data: any, type, orderList) {
    if (orderList) {
      this.orderList = orderList ? orderList : this.orderList;
      this.disableCheck=false;
    }
    var updateOrder = _.find(this.orderList.orderDetails.prodEntryList, ['id', data.id]);
    var previousValue = 0;
    if (updateOrder) {
      previousValue = +updateOrder[type];
      if ((+data.updateCases) == 0 && (+data.updateEaches) == 0) {
        _.remove(this.orderList.orderDetails.prodEntryList, (product: any) => {
          return (product.id == data.id);
        });
      } else {
        updateOrder[type] = data[type];
      }
    }
    else {
      this.orderList.orderDetails.prodEntryList.unshift(data);
    }
    this.updateCurrentOrderList(data, type, previousValue);
  }

  enteredProduct(productId) {
    this.productFound=false;
    if (productId) {
      this.quickEntryProduct = this.productService.getProductById(productId);
      if (!this.quickEntryProduct) {
        this.productFound = false;
      }
    }

  }

  quickEntryUpdate() {
    var updateOrder = _.find(this.orderList.orderDetails.quickEntryList, ['id', this.quickEntryProduct.id]);
    if (updateOrder) {
      if (updateOrder.priceCases) {
        let value = (+this.orderList.orderDetails.updateCases) + (+this.quickEntryProduct.updateCases);
        this.orderList.orderDetails.updateCases = value;
      }
      if (updateOrder.priceEaches) {
        let value = (+this.orderList.orderDetails.updateEaches) + (+this.quickEntryProduct.updateEaches);
        this.orderList.orderDetails.updateEaches = value;
      }
    }
    else {
      this.orderList.orderDetails.quickEntryList.unshift(this.quickEntryProduct);
      if (this.quickEntryProduct.updateCases) {
        let value = +this.quickEntryProduct.updateCases;
        this.orderList.orderDetails.updateCases += value;
      }
      if (this.quickEntryProduct.priceEaches) {
        let value = +this.quickEntryProduct.updateEaches;
        this.orderList.orderDetails.updateEaches += value;
      }
    }
    this.updateCurrentOrderFromQuickEntry(this.quickEntryProduct);

    this.quickEntryProduct = null;

    this.productFound = true;
    this.calculateTotalPrice();
    (document.querySelector("#productId")as HTMLInputElement).value="";
  }
  updateCurrentOrderFromQuickEntry(data) {
    var updateOrder = _.find(this.currentOrderList, ['id', data.id]);
    if (updateOrder) {
      if (updateOrder.priceCases) {
        let value = (+updateOrder.updateCases) + (+data.updateCases)
        updateOrder.updateCases = value;
      }
      if (updateOrder.priceEaches) {
        let value = (+updateOrder.updateEaches) + (+data.updateEaches);
        updateOrder.updateEaches = value;
      }
    }
    else {
      this.currentOrderList.unshift(data);
    }
  }
  updateCurrentOrderList(data, type, previousValue) {
    var updateOrder = _.find(this.currentOrderList, ['id', data.id]);
    if (updateOrder) {
      this.orderList.orderDetails[type] = this.orderList.orderDetails[type] - previousValue;
      this.orderList.orderDetails[type] += +(data[type]);
      if ((+data.updateCases) == 0 && (+data.updateEaches) == 0) {
        _.remove(this.currentOrderList, (product: any) => {
          return (product.id == data.id);
        });
      } else {
        var value = +updateOrder[type];
        value = value - previousValue;
        updateOrder[type] = value + (+(data[type]));
      }
    }
    else if ((+data.updateCases) !== 0 || (+data.updateEaches) !== 0) {
      this.currentOrderList.unshift(_.clone(data));
      this.orderList.orderDetails[type] += +(data[type]);
    }
    this.calculateTotalPrice()
  }
  calculateTotalPrice() {
    this.totalPrice = 0;
    _.each(this.currentOrderList, (value) => {
      let temp = 0;
      if (value.updateCases) {
        temp += (+value.updateCases) * (+value.priceCases);
      }
      if (value.updateEaches) {
        temp += (+value.updateEaches) * (+value.priceEaches);
      }
      this.totalPrice += temp;
    })
  }
}